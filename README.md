# isomnt

Mount ISO images as regular user.

----

### Installation

```txt
$ curl -s https://pkg.bfi.io/key | sudo apt-key add -
$ echo 'deb [arch=amd64] https://pkg.bfi.io/ubuntu/focal/main focal main' > /etc/apt/sources.list.d/bfi.list
$ apt update
$ apt install isomnt -y
```

Match `ubuntu/focal` and `focal` to your Debian flavor, e.g.
`debian/buster` and `buster`.

### Usage

```txt
$ isomnt -h
```

or:

![screenshot on dolphin](screenshot.png "screenshot on dolphin")
