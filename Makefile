
VTAG := $(shell \
	git describe --abbrev=0 2>/dev/null | \
	sed 's:^v::' \
)
ifeq ($(VTAG),)
	VTAG := devel
endif

all: deb

check: tests

deb:
	@if test x$(VTAG) = xdevel; then \
		echo "Version number not found."; \
		exit 1; \
	fi
	@sed "s:VERSION:$(VTAG):;" debian/control > ./.control.log
	fpm \
		-t deb -f \
		--deb-custom-control=./.control.log \
		--deb-no-default-config-files \
		-v $(VTAG) \
		-n isomnt \
		-s dir \
		./bin/=/usr/bin/ \
		./share/=/usr/share/

tests:
	@# no-op

clean:
	rm -f *.deb ./.control.log

.PHONY: deb
